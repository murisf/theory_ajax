<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>CS4010 Fall 2014: Theory Example</title>
<script type="text/javascript">
	window.MathJax = {
		tex2jax : {
			inlineMath : [ [ '$', '$' ], [ "\\(", "\\)" ] ],
			processEscapes : true
		}
	};
</script>
<script type="text/javascript"
	src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
	
</script>
<style type="text/css">
body {
	color: #333333;
	margin: 0 auto;
	font-size: 1em;
	font-family: Verdana, "Verdana CE", Arial, "Arial CE",
		"Lucida Grande CE", lucida, "Helvetica CE", sans-serif;
	text-align: center;
}

.baseFont {
	font-size: 1.1em;
	font-family: helvetica, arial, sans-serif;
}

.borderall1 {
	border: 1px solid lightblue;
}

.indiv {
	position: relative;
	width: 27em;
	height: 10em;
	padding: 1em;
	overflow: auto;
}

.inframe {
	border: 2px solid;
	position: relative;
	margin: 0 auto;
	top: 5px;
	color: black;
	border-color: #00BFFF;
	width: 30em;
	display: block;
	padding: 1.5em;
	text-align: left;
	background-color: #FFF;
}
</style>
</head>
<body>
	<div class="inframe baseFont">
		<table>
			<tr>
				<td>List of theory items</td>
			</tr>
			<tr>
				<td>
					<div class="indiv borderall1">
						<table>
							<c:forEach items="${theoryList}" var="theory">
								<tr>
									<td>${theory.tid}</td>
									<td><c:choose>
											<c:when test="${theory.tid == tid}"><b>${theory.theory_title}</b></c:when>
											<c:otherwise><a href="ContentServlet?tid=${theory.tid}">${theory.theory_title}</a></c:otherwise>
										</c:choose>
									</td>
								</tr>
							</c:forEach>
						</table>
					</div>
				</td>
			</tr>
			<tr>
				<td>Content:</td>
			</tr>
			<tr>
				<td>
					<div id="contarea" class="indiv borderall1">
						<b>${tid} &nbsp; ${title}</b>
						<p></p>${cont}
					</div>
				</td>
			</tr>
		</table>
		<p></p>
		<p></p>
	</div>
</body>
</html>