package edu.umsl.theory.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.umsl.theory.dao.TheoryDao;
import edu.umsl.theory.dao.jdbc.JdbcTheoryDaoImpl;

@WebServlet("/ContentServlet")
public class ContentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ContentServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String cont = "";
		String tidstr = request.getParameter("tid");
		
		RequestDispatcher dispatcher = 
                request.getRequestDispatcher("list.jsp");

		if (tidstr != null && tidstr.length() > 0) {
			TheoryDao thydao = new JdbcTheoryDaoImpl();
			cont = thydao.getContentById(Integer.parseInt(tidstr));
			
			String[] contArr = cont.split("@==@");
			
			request.setAttribute("tid", contArr[0]);
			request.setAttribute("title", contArr[1]);
			request.setAttribute("cont", contArr[2]);
		}

		dispatcher.forward(request, response);
	}

}
