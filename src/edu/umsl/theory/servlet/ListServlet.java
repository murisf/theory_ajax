package edu.umsl.theory.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import edu.umsl.theory.bean.TheoryBean;
import edu.umsl.theory.dao.TheoryDao;
import edu.umsl.theory.dao.jdbc.JdbcTheoryDaoImpl;

@WebServlet("/ListServlet")
public class ListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ListServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		
		List<TheoryBean> theoryList = (List<TheoryBean>) session.getAttribute("theoryList");
		
		if (theoryList == null || theoryList.size() == 0) {
			TheoryDao thydao = new JdbcTheoryDaoImpl();
			theoryList = thydao.listAllTheoryTitles();
			
			session.setAttribute("theoryList", theoryList);
		}
		
		response.sendRedirect("list.jsp");
	}

}
