package edu.umsl.theory.bean;

public class TheoryBean {
	
	private int tid;
	private String theory_title;
	
	public int getTid() {
		return tid;
	}
	
	public void setTid(int tid) {
		this.tid = tid;
	}
	
	public String getTheory_title() {
		return theory_title;
	}
	
	public void setTheory_title(String theory_title) {
		this.theory_title = theory_title;
	}
}
