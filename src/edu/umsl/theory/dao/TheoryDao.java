package edu.umsl.theory.dao;

import java.util.List;

import edu.umsl.theory.bean.TheoryBean;

public interface TheoryDao {
	
	public List<TheoryBean> listAllTheoryTitles();
	
	public String getContentById(int tid);

}
