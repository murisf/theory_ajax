package edu.umsl.theory.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.InitialContext;
import javax.sql.DataSource;

import edu.umsl.theory.bean.TheoryBean;
import edu.umsl.theory.dao.TheoryDao;

public class JdbcTheoryDaoImpl implements TheoryDao {

	public List<TheoryBean> listAllTheoryTitles() {
		List<TheoryBean> titlesList = new ArrayList<TheoryBean>();

		try {
			InitialContext ic = new InitialContext();
			DataSource source = (DataSource) ic
					.lookup("java:comp/env/jdbc/theorydb");
			Connection connection = source.getConnection();

			PreparedStatement results = connection
					.prepareStatement("SELECT tid, title FROM theory ORDER BY tid");
			ResultSet resultsRS = results.executeQuery();

			while (resultsRS.next()) {
				TheoryBean tb = new TheoryBean();

				tb.setTid(resultsRS.getInt(1));
				tb.setTheory_title(resultsRS.getString(2));

				titlesList.add(tb);
			}

			resultsRS.close();
			results.close();
			connection.close();
		} catch (Exception sqle) {
			sqle.printStackTrace();
		}

		return titlesList;
	}

	public String getContentById(int tid) {
		String content = "";

		try {
			InitialContext ic = new InitialContext();
			DataSource source = (DataSource) ic
					.lookup("java:comp/env/jdbc/theorydb");
			Connection connection = source.getConnection();

			PreparedStatement results = connection
					.prepareStatement("SELECT title, content FROM theory WHERE tid = ?");
			results.setInt(1, tid);
			ResultSet resultsRS = results.executeQuery();

			if (resultsRS.next()) {
				content = "" + tid + "@==@" + resultsRS.getString(1) + "@==@"
						+ resultsRS.getString(2);
			}

			resultsRS.close();
			results.close();
			connection.close();
		} catch (Exception sqle) {
			sqle.printStackTrace();
		}

		return content;
	}

}
